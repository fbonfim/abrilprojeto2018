-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: bdabril
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `cod_prod` int(11) NOT NULL AUTO_INCREMENT,
  `nome_prod` varchar(100) NOT NULL,
  `descricao_prod` varchar(500) NOT NULL,
  `preco_prod` varchar(20) NOT NULL,
  PRIMARY KEY (`cod_prod`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` VALUES (5,'Revista Quatro Rodas','Revista 4 Rodas edição 2018','R$ 10'),(6,'Revista Veja','Revista veja 2018','R$ 10,90'),(7,'Revista Super Interessantes','Revista Super Interessantes versão 2018','R$ 8,90'),(8,'Livro O Destino de Miguel','Este livro é o livro mais famoso do YouTube, falando da experiência de Miguel um ser iluminado','R$ 100,00'),(9,'Livro Alex Rider','Livro do mais famoso jovem Agente Secreto, está em uma nova missão para salvar o mundo','R$ 200,00'),(10,'Livro Mr.Bean','Este Livro conta as melhores aventuras do SR. Bean pelo mundo','R$ 50,00'),(17,' Livro Alex Cros',' Livro do Alex Crows em mais uma de suas aventuras',' R$ 20,00'),(18,' Livro O Pequeno Príncipe',' Um livro que inspirou muitas crianças antigamente, está de volta, trazendo toda a inocência de um Pequeno Príncipe',' R$ 10,00'),(19,'Livro A volta Dos Que Não Foram',' Só Lendo para crer',' R$ 60,00');
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-04  5:32:03
