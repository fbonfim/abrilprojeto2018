/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

/**
 *
 * @author Felipe Matheus
 */
public class Produtos {
    
    private int id;
    private String descricao;
    
    public Produtos(){
        
    }
    public Produtos (int id, String descricao){
        
        this.id = id;
        this.descricao = descricao;
    }
    
    public int getID(){
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
        public String getDes(){
        return descricao;
    }
    
    public void setDes(String descricao){
        this.descricao = descricao;
    }
    public String toString(){
        
        return descricao;
    }
}
