/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Felipe Matheus
 */
public class ProdutoData extends conectar{
    
    public ProdutoData() throws Exception{}
    public Vector<Produtos> listarProdutos()  throws Exception{
        conectar cc5 = new conectar();
        Connection cn5 = cc5.conexion();
        String url = "SELECT * FROM produto";
        Vector<Produtos> products = new Vector<Produtos>();
        PreparedStatement ps4 =  cn5.prepareStatement(url);
        ResultSet rs = ps4.executeQuery();
        while(rs.next()){
            Produtos obj = new Produtos();
            obj.setDes(rs.getString("nome_prod"));
            products.add(obj);
        }
        return products;
                }
  
    
}
